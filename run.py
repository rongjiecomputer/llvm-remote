import argparse
import json
import os
import shutil
import subprocess
import sys
import tempfile
import urllib.request, urllib.error
import zipfile

# https://cs.chromium.org/chromium/src/tools/clang/scripts/update.py

CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
COMMITS_JSON = os.path.join(CURRENT_DIR, "commits.json")

SRC_DIR = os.path.abspath(os.path.join(CURRENT_DIR, "..", "src"))
INSTALL_DIR = os.path.abspath(os.path.join(CURRENT_DIR, "..", "install"))
BUILD_DIR = os.path.abspath(os.path.join(CURRENT_DIR, "..", "build"))
DEPS_DIR = os.path.abspath(os.path.join(CURRENT_DIR, "..", "deps"))
LLVM_DEPS = os.path.join(DEPS_DIR, "llvm")
NINJA_DEPS = os.path.join(DEPS_DIR, "ninja")
LLVM_CONFIG = os.path.join(LLVM_DEPS, "bin", "llvm-config.exe")

LLVM_GIT_URL = "https://git.llvm.org/git/"
APPVEYOR_URL = "https://ci.appveyor.com/api/projects/rongjiecomputer/"
SEVEN_ZIP = r"C:\Program Files\7-Zip\7z.exe"

def GithubApi2(query):
  req = urllib.request.Request("https://api.github.com" + query)
  res = urllib.request.urlopen(req)
  data = res.read()
  return data.decode("utf8")

def RunCommand(command, fail_hard=True, **kwargs):
  print("Running ", command)
  if subprocess.call(command, env=None, **kwargs) == 0:
    return True
  print("Failed")
  if fail_hard:
    sys.exit(1)
  return False

def IsAppveyor():
  return "APPVEYOR" in os.environ

def DownloadFile(url, output_file):
  sys.stdout.write("Downloading " + url)
  sys.stdout.flush()
  response = urllib.request.urlopen(url)
  total_size = int(response.getheader('Content-Length').strip())
  received = 0
  dots_printed = 0
  while True:
    chunk = response.read(4096)
    if not chunk:
      break
    output_file.write(chunk)
    received += len(chunk)
    num_dots = (10 * received) // total_size
    sys.stdout.write('.' * (num_dots - dots_printed))
    sys.stdout.flush()
    dots_printed = num_dots
  if received != total_size:
    raise urllib.error.URLError("only got %d of %d bytes" % (received, total_size))
  print("Done")

def EnsureDirExists(path):
  if not os.path.exists(path):
    print("Creating directory " + path)
    os.makedirs(path)

def pinGitCommits():
  projects = ["llvm", "clang", "lld", "compiler-rt"]
  obj = {}
  for project in projects:
    res = GithubApi2("/repos/llvm-mirror/{}/commits/master".format(project))
    data = json.loads(res)
    obj[project] = data["sha"]
  with open(COMMITS_JSON, "w") as f:
    f.write(json.dumps(obj))
  if not IsAppveyor():
    RunCommand(["git", "add", COMMITS_JSON])
    RunCommand(["git", "commit", "-m", "[ci skip] pinGitCommits"])
    RunCommand(["git", "push"])

def loadGitCommit(project):
  with open(COMMITS_JSON, "r") as f:
    obj = json.loads(f.read())
  return obj[project]

def gitClone(project):
  hash = loadGitCommit(project)
  if SyncArchive(DEPS_DIR, project + '-src',
                'https://github.com/llvm-mirror/{}/archive/{}.zip'.format(project, hash)):
    if os.path.exists(SRC_DIR):
      RunCommand(['rmdir', SRC_DIR])
    RunCommand(['mklink', '/J', SRC_DIR, os.path.join(DEPS_DIR, project + '-' + hash)], shell=True)

def SyncArchive(out_dir, name, url):
  stamp_file = os.path.join(out_dir, name + '-stamp.txt')
  if os.path.isdir(out_dir):
    if os.path.isfile(stamp_file):
      with open(stamp_file) as f:
        stamp_url = f.read().strip()
      if stamp_url == url:
        print('%s directory already exists' % name)
        return False
    print('%s directory exists but is not up-to-date' % name)
  print('Downloading %s from %s' % (name, url))

  try:
    res = urllib.request.urlopen(url)
    #print('URL: %s' % res.geturl())

    ext = os.path.splitext(url)[-1]
    if ext == '.7z':
      tmp = tempfile.NamedTemporaryFile(delete=False)
      tmp.write(res.read())
      tmp.flush()
      tmp.close()
      RunCommand(['7z', 'x', tmp.name, '-o' + out_dir, '-aoa'], stdout=subprocess.DEVNULL)
      os.remove(tmp.name)
    else:
      with tempfile.NamedTemporaryFile() as tmp:
        tmp.write(res.read())
        tmp.flush()
        tmp.seek(0)
        if ext == '.zip':
          with zipfile.ZipFile(tmp, 'r') as zip:
            zip.extractall(path=out_dir)
  except urllib.error.URLError as e:
    print('Error downloading %s: %s' % (url, e))
    raise

  with open(stamp_file, 'w') as f:
    f.write(url + '\n')
  return True

def SyncNinja():
  SyncArchive(NINJA_DEPS, 'Ninja',
              'https://github.com/ninja-build/ninja/releases/download/v1.8.2/ninja-win.zip')
  os.environ['PATH'] = NINJA_DEPS + os.pathsep + os.getenv('PATH')

def SyncPrebuiltLLVM(project, use_clang=False, use_lld=False, use_lto=False):
  if project != 'llvm' or use_lto:
    SyncArchive(LLVM_DEPS, 'llvm', APPVEYOR_URL + 'llvm-remote-v2fhw/artifacts/llvm.7z')
  if use_clang:
    SyncArchive(LLVM_DEPS, 'clang', APPVEYOR_URL + 'llvm-remote-tthe7/artifacts/clang.7z')
  if use_lld or use_lto:
    SyncArchive(LLVM_DEPS, 'lld', APPVEYOR_URL + 'llvm-remote-yjyp1/artifacts/lld.7z')

def downloadToolchain():
  dest_dir = os.path.join(os.environ['USERPROFILE'], 'Tools', 'llvm')
  pairs = [
    ('llvm-remote-v2fhw/artifacts/llvm.7z', 'llvm.7z'),
    ('llvm-remote-tthe7/artifacts/clang.7z', 'clang.7z'),
    ('llvm-remote-yjyp1/artifacts/lld.7z', 'lld.7z'),
  ]

  for path, filename in pairs:
    with open(filename, 'wb') as f:
      DownloadFile(APPVEYOR_URL + path, f)
    RunCommand([SEVEN_ZIP, 'x', filename, '-aoa', '-o' + dest_dir, '@list.txt'], stdout=subprocess.DEVNULL)

def cmakeConfigure(project, use_clang=False, use_lld=False, use_lto=False, generate_pdb=False):
  cflags = ['/Gy', '/Gw', '/GS-']
  ldflags = ['/OPT:REF', '/OPT:ICF']

  if generate_pdb:
    cflags += ['/Zi']
    ldflags = ['/DEBUG'] + ldflags

  cmd = ['cmake', '-B' + BUILD_DIR, '-H' + SRC_DIR, '-GNinja', '-DCMAKE_BUILD_TYPE=Release',
         '-DCMAKE_INSTALL_PREFIX=' + INSTALL_DIR.replace('\\', '/'),
         '-DLLVM_INCLUDE_TESTS=OFF',
         '-DLLVM_TARGETS_TO_BUILD=X86',
         '-DCMAKE_C_FLAGS=' + ' '.join(cflags),
         '-DCMAKE_CXX_FLAGS=' + ' '.join(cflags),
         '-DCMAKE_EXE_LINKER_FLAGS=' + ' '.join(ldflags),
         '-DCMAKE_SHARED_LINKER_FLAGS=' + ' '.join(ldflags),
         '-DCMAKE_MODULE_LINKER_FLAGS=' + ' '.join(ldflags),
        ]

  if project == 'llvm':
    cmd += ['-DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD=WebAssembly',
            '-DLLVM_INCLUDE_EXAMPLES=OFF',
           ]
  else:
    if project == 'clang':
      cmd += ['-DLLVM_CONFIG=' + LLVM_CONFIG.replace('\\', '/')]
    else: # lld, compiler-rt
      cmd += ['-DLLVM_CONFIG_PATH=' + LLVM_CONFIG.replace('\\', '/')]

  if use_clang:
    cc = os.path.join(LLVM_DEPS, 'bin', 'clang-cl.exe').replace('\\', '/')
    cmd += ['-DCMAKE_C_COMPILER=' + cc, '-DCMAKE_CXX_COMPILER=' + cc]
  else:
    cmd += ['-DCMAKE_C_COMPILER=cl.exe', '-DCMAKE_CXX_COMPILER=cl.exe']

  if use_lld or use_lto:
    link = os.path.join(LLVM_DEPS, 'bin', 'lld-link.exe')
    cmd += ['-DCMAKE_LINKER=' + link.replace('\\', '/')]

  if use_lto:
    ar = os.path.join(LLVM_DEPS, 'bin', 'llvm-ar.exe')
    randlib = os.path.join(LLVM_DEPS, 'bin', 'llvm-ranlib.exe')
    cmd += ['-DCMAKE_AR=' + ar.replace('\\', '/'),
            '-DCMAKE_RANLIB=' + ranlib.replace('\\', '/'),
            '-DCMAKE_ENABLE_LTO=thin',
           ]

  if project == "lld":
    RunCommand(['git', 'apply', os.path.join(CURRENT_DIR, "lld.diff")], fail_hard=False, cwd=SRC_DIR)

  RunCommand(cmd)

def buildLLVM():
  RunCommand(['ninja', '-C', BUILD_DIR])
  RunCommand(['ninja', '-C', BUILD_DIR, 'install'])

  if IsAppveyor():
    RunCommand(['7z', 'a', 'llvm.7z', '.', '-t7z'], cwd=INSTALL_DIR, stdout=subprocess.DEVNULL)
    RunCommand(['appveyor', 'PushArtifact', 'llvm.7z'], cwd=INSTALL_DIR)

def buildClang():
  RunCommand(['ninja', '-C', BUILD_DIR, 'clang', 'clang-resource-headers'])
  RunCommand(['ninja', '-C', BUILD_DIR, 'install-clang', 'install-clang-resource-headers'])

  if IsAppveyor():
    RunCommand(['7z', 'a', 'clang.7z', 'bin\\clang-cl.exe', 'lib'], cwd=INSTALL_DIR, stdout=subprocess.DEVNULL)
    RunCommand(['appveyor', 'PushArtifact', 'clang.7z'], cwd=INSTALL_DIR)

def buildLld():
  RunCommand(['ninja', '-C', BUILD_DIR, 'lld'])
  RunCommand(['ninja', '-C', BUILD_DIR, 'install-lld'])
  #RunCommand(['dir', '/S', '/B', BUILD_DIR], shell=True)
  RunCommand(['copy', os.path.join(BUILD_DIR, "bin", "*.pdb"), os.path.join(INSTALL_DIR, "bin")], shell=True)

  if IsAppveyor():
    RunCommand(['7z', 'a', 'lld.7z', 'bin\\lld-link.exe', 'bin\\*.pdb'], cwd=INSTALL_DIR, stdout=subprocess.DEVNULL)
    RunCommand(['appveyor', 'PushArtifact', 'lld.7z'], cwd=INSTALL_DIR)

def buildCompilerRt():
  RunCommand(['ninja', '-C', BUILD_DIR])
  RunCommand(['ninja', '-C', BUILD_DIR, 'install'])

  if IsAppveyor():
    RunCommand(['7z', 'a', 'compiler-rt.7z', '.'], cwd=INSTALL_DIR, stdout=subprocess.DEVNULL)
    RunCommand(['appveyor', 'PushArtifact', 'compiler-rt.7z'], cwd=INSTALL_DIR)

build = {
  'llvm': buildLLVM,
  'clang': buildClang,
  'lld': buildLld,
  'compiler-rt': buildCompilerRt,
}

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('action', type=str)
  parser.add_argument('--use_clang', action='store_true')
  parser.add_argument('--use_lld', action='store_true', default=True)
  parser.add_argument('--use_lto', action='store_true')
  parser.add_argument('--generate_pdb', action='store_true')

  FLAGS, unparsed = parser.parse_known_args()
  if FLAGS.action == "pinGitCommits":
    pinGitCommits()
  elif FLAGS.action == "download":
    downloadToolchain()
  else:
    EnsureDirExists(DEPS_DIR)
    SyncNinja()
    if FLAGS.action.startswith('build-'):
      project = FLAGS.action[6:]
      gitClone(project)
      SyncPrebuiltLLVM(project, FLAGS.use_clang, FLAGS.use_lld, FLAGS.use_lto)
      cmakeConfigure(project, FLAGS.use_clang, FLAGS.use_lld, FLAGS.use_lto,
                     FLAGS.generate_pdb)
      build[project]()
